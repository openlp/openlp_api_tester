# -*- coding: utf-8 -*-

##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2021 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
from colorama import init, Fore
init(autoreset=True)


def print_text(text: str):
    print(Fore.LIGHTMAGENTA_EX + f'[*] {text}')


def print_error(text: str):
    print(Fore.RED + f'[-] {text}')


def print_ok(text: str):
    print(Fore.GREEN + f'[_] {text}')


def print_warn(text: str):
    print(Fore.YELLOW + f'[?] {text}')


def print_info(text: str):
    print(Fore.MAGENTA + f'[!] {text}')


def print_debug(text: str):
    print(Fore.CYAN + f'[#] {text}')
